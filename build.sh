#!/bin/bash

set -x

# Setup ROS
source /opt/ros/humble/setup.bash

colcon build --cmake-args ' -DBUILD_TESTING=1' --packages-select demo
