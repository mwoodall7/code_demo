#!/bin/bash
set -x

PROJECT_NAME="code_demo"

# Parse options
# - l > update "latest" of each image
OPTIONS=":l"

UPDATE_LATEST=false

while getopts ${OPTIONS} o; do
    case ${o} in
        l)
            echo "Set to update latest tag of images"
            UPDATE_LATEST=true
            ;;
        ?)
            echo "Invalid argument: -${OPTARG}"
            exit 1
            ;;
    esac
done

# Build development docker image
VERSION_DEV=0.0.1

docker build -t ${PROJECT_NAME}_dev:v${VERSION_DEV} -f Dockerfile.dev .

if [ ${UPDATE_LATEST} == true ]; then
    docker tag ${PROJECT_NAME}_dev:v${VERSION_DEV} ${PROJECT_NAME}_dev:latest
fi

# Build desktop runtime image
VERSION_RUNTIME=0.0.1

# Build target docker image
VERSION_TARGET=0.0.1


# Clean up dangling docker images
DANGLING=$(docker images --filter "dangling=true" -q --no-trunc)
if [ -z "$DANGLING" ]; then
    echo "No dangling images to remove"
else
    docker rmi ${DANGLING}
fi
