#!/bin/bash

# Current has to be run in container as root. TODO: Figure out why

# Ensure ROS2 packages are available
source /opt/ros/humble/setup.bash
source src/install/setup.bash

colcon test --ctest-args tests demo --event-handlers console_direct+
