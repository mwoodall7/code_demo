#include <gtest/gtest.h>
#include "image_source.hpp"
#include "image_sub_util.hpp"
#include "rclcpp/rclcpp.hpp"
#include <unistd.h>

// Note this test requires the webcam to be connected to pull and image as there is not a stub
// created to fulfill the callback needs of the ImageSource
TEST(demo, integration_publish_image_test) {
    rclcpp::init(0, nullptr);
    // Create rclcpp executor
    auto exec = rclcpp::executors::SingleThreadedExecutor();

    // Create the image source node
    auto node_name = "Webcam";
    auto node = std::make_shared<ImageSource>(node_name);

    // Create a subscriber node to check if an image is published
    auto sub_node_name = "TestImageSub";
    auto sub_node = std::make_shared<TestImageSub>(sub_node_name, "Webcam_feed");

    // Add nodes to the executor
    exec.add_node(node);
    exec.add_node(sub_node);

    // Spin the executor in single increments for a bit to allow the system to acquire and start grabbing images
    for (auto i = 0; i < 100; ++i) {
        exec.spin_once();
        usleep(50000);
    }

    auto sub_frame_height = sub_node->last_image_height;
    auto sub_frame_width = sub_node->last_image_width;
    rclcpp::shutdown();


    // Check that the pulled image is bigger than 100x100 pixels given the webcam quality
    ASSERT_GT(sub_frame_height, 100);
    ASSERT_GT(sub_frame_width, 100);
}
