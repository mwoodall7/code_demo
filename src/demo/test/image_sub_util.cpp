#include "image_sub_util.hpp"

TestImageSub::TestImageSub(std::string node_name, std::string topic_name) : Node(node_name) {
    image_sub = this->create_subscription<sensor_msgs::msg::Image>(topic_name, 10, 
        [this](const sensor_msgs::msg::Image::ConstSharedPtr& msg){ test_image_recv_cb(msg); });
}

void TestImageSub::test_image_recv_cb(const sensor_msgs::msg::Image::ConstSharedPtr& msg) {
    last_image_height = msg->height;
    last_image_width = msg->width;
}
