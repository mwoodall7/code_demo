#pragma once

#include <string>
#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/image.hpp>


class TestImageSub : public rclcpp::Node {
    public:
        TestImageSub(std::string node_name, std::string topic_name);

        // Making public just to make it easier to access parts of the image message within the test
        int last_image_height;
        int last_image_width;

    private:
        void test_image_recv_cb(const sensor_msgs::msg::Image::ConstSharedPtr& msg);
        rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr image_sub;
};
