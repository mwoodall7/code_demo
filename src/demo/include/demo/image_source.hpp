#pragma once

#include <string>
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/image.hpp"
#include "opencv2/opencv.hpp"


class ImageSource : public rclcpp::Node {
    public:
        ImageSource(std::string node_name);
        ~ImageSource();

    private:
        cv::VideoCapture input;
        int frame_w;
        int frame_h;

        rclcpp::TimerBase::SharedPtr image_publish_timer;
        rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr image_publisher;

        void image_publish_timer_cb();
};
