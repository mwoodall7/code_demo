#include "image_source.hpp"
#include <chrono>
#include <cv_bridge/cv_bridge.h>
#include "std_msgs/msg/header.hpp"
#include <image_transport/image_transport.hpp>

using namespace std::chrono_literals;

ImageSource::ImageSource(std::string node_name) : Node(node_name) {
    input.open(0);
    frame_w = input.get(cv::CAP_PROP_FRAME_WIDTH);
    frame_h = input.get(cv::CAP_PROP_FRAME_HEIGHT);

    image_publisher = this->create_publisher<sensor_msgs::msg::Image>("Webcam_feed", 30);
    // Use 34ms to get approx 30fps
    image_publish_timer = this->create_wall_timer(34ms, [this]() { image_publish_timer_cb(); });
}

ImageSource::~ImageSource() {
    input.release();
}

void ImageSource::image_publish_timer_cb() {
    cv::Mat frame;

    // Check for a valid image frame
    if (input.grab()) {
        // Get the frame and store it in the cv::Mat
        input.retrieve(frame);

        // Create image message to publish on the ros network
        sensor_msgs::msg::Image::SharedPtr msg = cv_bridge::CvImage(std_msgs::msg::Header(), "bgr8", frame).toImageMsg();

        image_publisher->publish(*msg.get());
    }
}
