#include "rclcpp/rclcpp.hpp"
#include "image_source.hpp"

auto main(int argc, char** argv)-> int {
    rclcpp::init(argc, argv);

    // Create ROS2 Node
    auto node_name = "Webcam";
    auto node = std::make_shared<ImageSource>(node_name);

    rclcpp::spin(node);
    rclcpp::shutdown();

    return 0;
}
