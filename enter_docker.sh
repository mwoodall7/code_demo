#!/bin/bash

DOCKER_IMAGE="code_demo_dev"

docker run --user $(id -u ${USER}):$(id -g ${USER}) --privileged -it --rm \
    -v .:/project \
    -v /etc/passwd:/etc/passwd:ro \
    -v /etc/shadow:/etc/shadow:ro \
    -v /etc/group:/etc/group:ro \
    ${DOCKER_IMAGE} /bin/bash
