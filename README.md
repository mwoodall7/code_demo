# code_demo
## Description
Small system built within the ROS2 Humble architecture to retrieve images from a Raspberry PI 4 using a generic Logitech Webcam and display them on the desktop.

## Requirements
Project is meant to be built/used on a linux machine with docker installed. Tested on Ubuntu 22.04 desktop and Ubuntu 22.04 for Raspberry Pi 4.

docker, docker.io, docker-registry, docker-buildx
qemu, qemu-user-static
build-essential
gcc-arm-linux-gnueabihf

## Highlights
### Build Process
 - Docker containerization
 - build scripts
 - Unit Tests (googletest)
 - Plan for CI/CD Integration

### Deployment
Docker image deployment
 - Utilize systemd to set a process start on boot up

### General Usage Steps
 - Outline steps to run on PI and Desktop to run functioning system


